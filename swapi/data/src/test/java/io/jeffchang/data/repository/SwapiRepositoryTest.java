package io.jeffchang.data.repository;

import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.jeffchang.core.util.logging.Logger;
import io.jeffchang.data.SwapiService;
import io.jeffchang.data.model.APIPerson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SwapiRepositoryTest {

    @SuppressWarnings("unchecked")
    @Test
    @Ignore("Going to leave this as a todo out of time constraints")
    public void verifyGetPeopleIsBeingCalled() {
        SwapiService api = mock(SwapiService.class);
        Call<PeopleResponse> mockedCall = mock(Call.class);

        when(api.getPeople(any())).thenReturn(mockedCall);

        doAnswer(invocation -> {
            Callback<PeopleResponse> callback = invocation.getArgument(0);

            List<APIPerson> people = new ArrayList<>();
            callback.onResponse(mockedCall, Response.success(new PeopleResponse(people)));
            return null;
        }).when(mockedCall).enqueue(any(Callback.class));

        Logger logger = mock(Logger.class);
        DefaultSwapiRepository repository = new DefaultSwapiRepository(logger, api);
        repository.getCharacters("darth");
//        repository.getCharacters("darth", new ApiCallback<List<APIPerson>>() {
//            @Override
//            public void onSuccess(@NonNull List<APIPerson> response) {
//                // no-op
//            }
//
//            @Override
//            public void onFailure(@NonNull APIError error) {
//                // no-op
//            }
//        });
        verify(api, times(1)).getPeople(any());
    }
}
