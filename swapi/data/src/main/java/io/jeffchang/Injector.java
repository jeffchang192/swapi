package io.jeffchang;


import com.squareup.moshi.Moshi;

import io.jeffchang.core.util.logging.AndroidLogger;
import io.jeffchang.core.util.logging.Logger;
import io.jeffchang.data.Constants;
import io.jeffchang.data.repository.DefaultSwapiRepository;
import io.jeffchang.data.SwapiService;
import io.jeffchang.data.repository.SwapiCore;
import io.jeffchang.data.repository.SwapiRepository;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

// Simple DI framework
// Runs across entire lifecycle of API.
public class Injector {

    private SwapiCore swapi;

    private MoshiConverterFactory converterFactory;
    private Retrofit retrofit;
    private SwapiService swapiService;
    public SwapiRepository swapiRepository;
    private Moshi moshi;
    public Logger logger;

    public Injector() {
        initDIGraph();
    }

    private void initDIGraph() {
        getLogger();
        getSwapiRepository(
                swapiApi(
                        getRetrofit(
                                getMoshiConverterFactory(
                                        getMoshi()
                                )
                        )
                ));
    }

    private void getLogger() {
        if (logger == null)
            logger = new AndroidLogger();
    }

    private void getSwapiRepository(SwapiService swapiService) {
        if (swapiRepository == null)
            swapiRepository = new DefaultSwapiRepository(logger, swapiService);
    }


    private Moshi getMoshi() {
        if (moshi == null) {
            moshi = new Moshi.Builder().build();
        }
        return moshi;
    }

    private MoshiConverterFactory getMoshiConverterFactory(Moshi moshi) {
        if (converterFactory == null)
            converterFactory = MoshiConverterFactory.create(moshi);
        return converterFactory;
    }

    private Retrofit getRetrofit(MoshiConverterFactory moshiConverterFactory) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.API_URL)
                    .addConverterFactory(moshiConverterFactory)
                    .build();
        }
        return retrofit;
    }

    private SwapiService swapiApi(Retrofit retrofit) {
        if (swapiService == null)
            swapiService = retrofit.create(SwapiService.class);
        return swapiService;
    }

}
