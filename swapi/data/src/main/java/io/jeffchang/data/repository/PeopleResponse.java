package io.jeffchang.data.repository;

import androidx.annotation.NonNull;

import com.squareup.moshi.Json;

import java.util.List;

import io.jeffchang.data.model.APIPerson;
import io.jeffchang.data.model.DomainMapper;


public class PeopleResponse implements DomainMapper<List<APIPerson>> {

    public PeopleResponse(List<APIPerson> people) {
        this.results = people;
    }

    @Json(name = "count")
    private int count;

    @Json(name = "results")
    private List<APIPerson> results;

    public int getCount() {
        return count;
    }

    @Override
    public @NonNull
    List<APIPerson> toDomainModel() {
        return results;
    }
}