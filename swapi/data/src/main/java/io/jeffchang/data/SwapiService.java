package io.jeffchang.data;


import io.jeffchang.data.model.APIFilm;
import io.jeffchang.data.model.APIPerson;
import io.jeffchang.data.repository.PeopleResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface SwapiService {

    @GET("/api/people")
    Call<PeopleResponse> getPeople(@Query("search") String user);

    @GET
    Call<APIFilm> getFilm(@Url String url);

    @GET
    Call<APIPerson> getCharacter(@Url String url);

}