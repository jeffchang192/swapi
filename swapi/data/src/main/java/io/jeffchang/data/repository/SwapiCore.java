package io.jeffchang.data.repository;

import io.jeffchang.Injector;

public class SwapiCore {

    private final SwapiRepository repository;

    private SwapiCore() {
        Injector injector = new Injector();
        repository = injector.swapiRepository;
    }

    public SwapiRepository getRepository() {
        return repository;
    }


    public static class Factory {

        public Factory() {
            // no-init
        }

        public SwapiCore build() {
            return new SwapiCore();
        }
    }
}
