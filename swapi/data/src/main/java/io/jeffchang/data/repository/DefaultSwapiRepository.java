package io.jeffchang.data.repository;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.jeffchang.core.util.logging.Logger;
import io.jeffchang.data.model.APIFilm;
import io.jeffchang.data.SwapiService;
import io.jeffchang.data.model.APIPerson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefaultSwapiRepository implements SwapiRepository {

    private final SwapiService swapiService;
    private final Logger logger;

    public DefaultSwapiRepository(Logger logger, SwapiService swapiService) {
        this.swapiService = swapiService;
        this.logger = logger;
    }

    @Override
    public CompletableFuture<List<APIPerson>> getCharacters(String query) {
        CompletableFuture<List<APIPerson>> completableFuture = new CompletableFuture<>();
        swapiService.getPeople(query).enqueue(new Callback<PeopleResponse>() {
            @Override
            public void onResponse(
                    @NonNull Call<PeopleResponse> call,
                    @NonNull Response<PeopleResponse> response) {
                if (response.body() == null) {
                    completableFuture.completeExceptionally(new NullPointerException());
                    return;
                }
                logger.debug("Received people list from API of size " + response.body().getCount());
                completableFuture.complete(response.body().toDomainModel());
            }

            @Override
            public void onFailure(
                    @NonNull Call<PeopleResponse> call,
                    @NonNull Throwable t) {
                logger.error("Error getting list of people", t);
                completableFuture.completeExceptionally(t);
            }
        });
        return completableFuture;
    }

    @Override
    public CompletableFuture<APIFilm> getFilm(String url) {
        CompletableFuture<APIFilm> completableFuture = new CompletableFuture<>();
        swapiService.getFilm(url).enqueue(new Callback<APIFilm>() {
            @Override
            public void onResponse(
                    @NonNull Call<APIFilm> call,
                    @NonNull Response<APIFilm> response) {
                if (response.body() == null) {
                    completableFuture.completeExceptionally(new NullPointerException());
                    return;
                }
                APIFilm APIFilm = response.body();
                logger.debug("Film Title: " + response.body().getTitle());
                completableFuture.complete(APIFilm);
            }

            @Override
            public void onFailure(
                    @NonNull Call<APIFilm> call,
                    @NonNull Throwable t) {
                logger.error("Failed to get films", t);
                completableFuture.completeExceptionally(t);
            }
        });
        return completableFuture;
    }

    @Override
    public CompletableFuture<APIPerson> getCharacter(String url) {
        CompletableFuture<APIPerson> completableFuture = new CompletableFuture<>();
        swapiService.getCharacter(url).enqueue(new Callback<APIPerson>() {
            @Override
            public void onResponse(
                    @NonNull Call<APIPerson> call,
                    @NonNull Response<APIPerson> response
            ) {
                if (response.body() == null) {
                    completableFuture.completeExceptionally(new NullPointerException());
                    return;
                }
                APIPerson person = response.body();
                logger.debug("Film Title: " + response.body().getName());
                completableFuture.complete(person);
            }

            @Override
            public void onFailure(
                    @NonNull Call<APIPerson> call,
                    @NonNull Throwable t) {
                logger.error("Failed to get films", t);
                completableFuture.completeExceptionally(t);
            }
        });
        return completableFuture;
    }
}
