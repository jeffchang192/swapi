package io.jeffchang.data.model;

import com.squareup.moshi.Json;

import java.util.List;

public class APIPerson {

    @Json(name = "films")
    private List<String> films;

    @Json(name = "homeworld")
    private String homeworld;

    @Json(name = "gender")
    private String gender;

    @Json(name = "skin_color")
    private String skinColor;

    @Json(name = "edited")
    private String edited;

    @Json(name = "created")
    private String created;

    @Json(name = "mass")
    private String mass;

    @Json(name = "vehicles")
    private List<String> vehicles;

    @Json(name = "url")
    private String url;

    @Json(name = "hair_color")
    private String hairColor;

    @Json(name = "birth_year")
    private String birthYear;

    @Json(name = "eye_color")
    private String eyeColor;

    @Json(name = "species")
    private List<String> species;

    @Json(name = "starships")
    private List<String> starships;

    @Json(name = "name")
    private String name;

    @Json(name = "height")
    private String height;

    public List<String> getFilms() {
        return films;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public String getGender() {
        return gender;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public String getEdited() {
        return edited;
    }

    public String getCreated() {
        return created;
    }

    public String getMass() {
        return mass;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public String getUrl() {
        return url;
    }

    public String getHairColor() {
        return hairColor;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public List<String> getSpecies() {
        return species;
    }

    public List<String> getStarships() {
        return starships;
    }

    public String getName() {
        return name;
    }

    public String getHeight() {
        return height;
    }

}