package io.jeffchang.data.util;

import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

import io.jeffchang.core.util.APIError;
import retrofit2.HttpException;

public class NetworkUtil {

    // CompletableFuture.allOf is used to 'join' all of the results and waits for all futures
    // in the list to finish before combining into a list.
    // https://stackoverflow.com/a/30026710
    public static <T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> futures) {
        return CompletableFuture.allOf(futures.toArray(new CompletableFuture<?>[0]))
                .thenApply(v ->
                        futures.stream()
                                .map(CompletableFuture::join)
                                .collect(Collectors.toList())
                );
    }

    // Used for error block from Retrofit calls
    public static APIError getAPIError(Throwable throwable) {
        if (throwable instanceof CompletionException) {
            return getCause(throwable.getCause());
        }
        return getCause(throwable);
    }

    public static APIError getCause(Throwable throwable) {
        APIError error = APIError.UNKNOWN;
        if (throwable instanceof HttpException) {
            int errorCode = ((HttpException) throwable).code();
            if (errorCode == 429) {
                error = APIError.RATE_LIMIT_EXCEEDED;
            }
        }
        if (throwable instanceof UnknownHostException) {
            error = APIError.NETWORK;
        }
        return error;
    }
}
