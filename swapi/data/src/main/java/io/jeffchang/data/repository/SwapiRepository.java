package io.jeffchang.data.repository;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.jeffchang.data.model.APIFilm;
import io.jeffchang.data.model.APIPerson;


public interface SwapiRepository {

    CompletableFuture<List<APIPerson>> getCharacters(String query);

    CompletableFuture<APIPerson> getCharacter(String url);

    CompletableFuture<APIFilm> getFilm(String url);

}
