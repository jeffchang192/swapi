package io.jeffchang.data.model;

import com.squareup.moshi.Json;

import java.util.List;

public class APIFilm {

    @Json(name = "edited")
    private String edited;

    @Json(name = "director")
    private String director;

    @Json(name = "created")
    private String created;

    @Json(name = "vehicles")
    private List<String> vehicles;

    @Json(name = "opening_crawl")
    private String openingCrawl;

    @Json(name = "title")
    private String title;

    @Json(name = "url")
    private String url;

    @Json(name = "characters")
    private List<String> characters;

    @Json(name = "episode_id")
    private int episodeId;

    @Json(name = "planets")
    private List<String> planets;

    @Json(name = "release_date")
    private String releaseDate;

    @Json(name = "starships")
    private List<String> starships;

    @Json(name = "species")
    private List<String> species;

    @Json(name = "producer")
    private String producer;

    public String getEdited() {
        return edited;
    }

    public String getDirector() {
        return director;
    }

    public String getCreated() {
        return created;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public String getOpeningCrawl() {
        return openingCrawl;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public List<String> getCharacters() {
        return characters;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public List<String> getPlanets() {
        return planets;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public List<String> getStarships() {
        return starships;
    }

    public List<String> getSpecies() {
        return species;
    }

    public String getProducer() {
        return producer;
    }
}
