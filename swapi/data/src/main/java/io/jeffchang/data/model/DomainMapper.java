package io.jeffchang.data.model;

import androidx.annotation.Nullable;

public interface DomainMapper<E> {

    @Nullable
    E toDomainModel();

}
