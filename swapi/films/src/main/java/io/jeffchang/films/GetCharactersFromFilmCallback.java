package io.jeffchang.films;

import androidx.annotation.NonNull;

import java.util.List;

import io.jeffchang.core.util.APIError;

public interface GetCharactersFromFilmCallback {

    void onSuccess(@NonNull List<String> films);

    void onFailure(@NonNull APIError error);

    void onComplete();

}