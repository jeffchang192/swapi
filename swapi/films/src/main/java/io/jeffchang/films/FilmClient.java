package io.jeffchang.films;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import io.jeffchang.core.util.APIError;
import io.jeffchang.core.util.ApiCallback;
import io.jeffchang.data.model.APIFilm;
import io.jeffchang.data.model.APIPerson;
import io.jeffchang.data.repository.SwapiCore;
import io.jeffchang.data.repository.SwapiRepository;
import io.jeffchang.core.util.Thread;

import static io.jeffchang.data.util.NetworkUtil.getAPIError;
import static io.jeffchang.data.util.NetworkUtil.sequence;


public class FilmClient {

    private final SwapiRepository repository;

    public FilmClient() {
        SwapiCore swapi = new SwapiCore.Factory().build();
        this.repository = swapi.getRepository();
    }

    public void getCharactersFromFilm(@NonNull String url,
                                      @NonNull GetCharactersFromFilmCallback getCharactersFromFilmCallback) {
        getCharactersFromFilm(url, new ApiCallback<List<String>>() {
            @Override
            public void onSuccess(@NonNull List<String> response) {
                Thread.runOnMainThread(() -> {
                    getCharactersFromFilmCallback.onSuccess(response);
                    getCharactersFromFilmCallback.onComplete();
                });
            }

            @Override
            public void onFailure(@NonNull APIError error) {
                Thread.runOnMainThread(() -> {
                    getCharactersFromFilmCallback.onFailure(error);
                    getCharactersFromFilmCallback.onComplete();
                });
            }
        });
    }

    private void getCharactersFromFilm(
            String url,
            ApiCallback<List<String>> getPeopleCallback
    ) {
        repository.getFilm(url)
                .thenCompose(this::getCharactersFromAPI)
                .whenCompleteAsync((people, throwable) -> {
                    if (throwable != null) {
                        APIError error = getAPIError(throwable);
                        getPeopleCallback.onFailure(error);
                        return;
                    }
                    getPeopleCallback.onSuccess(people);
                });
    }

    private CompletableFuture<List<String>> getCharactersFromAPI(APIFilm APIFilm) {
        List<CompletableFuture<APIPerson>> filmFutures = APIFilm.getCharacters()
                .stream()
                .map(repository::getCharacter)
                .collect(Collectors.toList());
        return sequence(filmFutures)
                .thenApply(apiPeople ->
                        apiPeople.stream()
                                .map(apiPerson -> apiPerson.getName())
                                .collect(Collectors.toList())
                );
    }
}
