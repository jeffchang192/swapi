package io.jeffchang.core.util;

import android.os.Handler;
import android.os.Looper;


public class Thread {

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static void runOnMainThread(Runnable function) {
        handler.post(function);
    }
}
