package io.jeffchang.core.data.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Character {

    private final String name;
    private List<Film> films = new ArrayList<>();

    public Character(String name, List<Film> films) {
        this.name = name;
        this.films = films;
    }
    public Character(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Film> getFilms() {
        return films;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Character character = (Character) o;
        return Objects.equals(name, character.name) &&
                Objects.equals(films, character.films);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, films);
    }
}