package io.jeffchang.core.util.logging;

public interface Logger {

    void error(String message, Throwable t);

    void info(String message);

    void debug(String message);

}