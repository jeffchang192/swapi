package io.jeffchang.core.util;

import androidx.annotation.NonNull;


public interface ApiCallback<T> {

    void onSuccess(@NonNull T response);

    void onFailure(@NonNull APIError error);

}