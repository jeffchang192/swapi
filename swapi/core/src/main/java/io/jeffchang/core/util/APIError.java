package io.jeffchang.core.util;

public enum APIError {
    NETWORK, RATE_LIMIT_EXCEEDED, UNKNOWN, DATA_MISSING
}
