package io.jeffchang.core.util.logging;


import android.util.Log;

public class AndroidLogger implements Logger {

    private String TAG = "SWAPI";

    @Override
    public void error(String message, Throwable t) {
        Log.e(TAG, message, t);

    }

    @Override
    public void info(String message) {
        Log.i(TAG, message);
    }

    @Override
    public void debug(String message) {
        Log.d(TAG, message);
    }

}
