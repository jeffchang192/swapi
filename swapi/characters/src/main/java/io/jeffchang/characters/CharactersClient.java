package io.jeffchang.characters;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

import io.jeffchang.core.data.model.Character;
import io.jeffchang.core.data.model.Film;
import io.jeffchang.core.util.APIError;
import io.jeffchang.core.util.ApiCallback;
import io.jeffchang.data.model.APIFilm;
import io.jeffchang.data.model.APIPerson;
import io.jeffchang.data.repository.SwapiCore;
import io.jeffchang.data.repository.SwapiRepository;
import io.jeffchang.core.util.Thread;

import static io.jeffchang.data.util.NetworkUtil.getAPIError;
import static io.jeffchang.data.util.NetworkUtil.sequence;


public class CharactersClient {

    private final SwapiRepository repository;

    public CharactersClient() {
        SwapiCore swapi = new SwapiCore.Factory().build();
        this.repository = swapi.getRepository();
    }

    public void getCharacters(@NonNull String query,
                              @NonNull GetCharactersCallback getCharactersCallback) {
        getCharacters(query, new ApiCallback<List<Character>>() {
            @Override
            public void onSuccess(@NonNull List<Character> response) {
                Thread.runOnMainThread(() -> {
                    getCharactersCallback.onSuccess(response);
                    getCharactersCallback.onComplete();
                });
            }

            @Override
            public void onFailure(@NonNull APIError error) {
                Thread.runOnMainThread(() -> {
                    getCharactersCallback.onFailure(error);
                    getCharactersCallback.onComplete();
                });
            }
        });
    }

    private void getCharacters(
            String query,
            ApiCallback<List<Character>> getPeopleCallback
    ) {
        repository.getCharacters(query)
                .thenCompose(this::getCharactersFromAPI)
                .whenCompleteAsync((people, throwable) -> {
                    if (throwable != null) {
                        APIError error = getAPIError(throwable);
                        getPeopleCallback.onFailure(error);
                        return;
                    }
                    getPeopleCallback.onSuccess(people);
                });
    }

    private CompletableFuture<List<Character>> getCharactersFromAPI(List<APIPerson> query) {
        List<CompletableFuture<Character>> people = query.stream()
                .map((Function<APIPerson, CompletableFuture<Character>>) apiPerson -> {
                    List<CompletableFuture<APIFilm>> filmFutures = apiPerson.getFilms()
                            .stream()
                            .map(repository::getFilm)
                            .collect(Collectors.toList());
                    return sequence(filmFutures)
                            .thenApply(films -> {
                                List<Film> filmList = films.stream()
                                        .map(apiFilm ->
                                                new Film(apiFilm.getTitle(), apiFilm.getUrl())
                                        )
                                        .collect(Collectors.toList());
                                return new Character(
                                        apiPerson.getName(),
                                        filmList
                                );
                            });
                })
                .collect(Collectors.toList());
        return sequence(people);
    }
}
