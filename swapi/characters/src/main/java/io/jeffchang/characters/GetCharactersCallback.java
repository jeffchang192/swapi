package io.jeffchang.characters;

import java.util.List;

import io.jeffchang.core.data.model.Character;
import io.jeffchang.core.util.APIError;


public interface GetCharactersCallback {

    void onSuccess(List<Character> characters);

    void onFailure(APIError error);

    void onComplete();

}
