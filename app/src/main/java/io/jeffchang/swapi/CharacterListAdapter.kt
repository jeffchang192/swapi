package io.jeffchang.swapi

import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.inSpans
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.jeffchang.core.data.model.Character
import io.jeffchang.swapi.databinding.ItemPersonBinding

class CharacterListAdapter(private val onPersonClicked: ((film: String, url: String) -> Unit)) :
    ListAdapter<Character, CharacterListAdapter.PeopleViewHolder>(PeopleDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleViewHolder {
        val binding = ItemPersonBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PeopleViewHolder(binding, onPersonClicked)
    }

    override fun onBindViewHolder(holder: PeopleViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PeopleViewHolder(
        private val binding: ItemPersonBinding,
        private val onUrlClicked: (film: String, url: String) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.descTextView.movementMethod = LinkMovementMethod.getInstance()
        }

        fun bind(character: Character) {
            binding.apply {
                personTextView.text = character.name
                // Sets fields or use default values.
                val span = SpannableStringBuilder()
                character.films.forEach {
                    span.click({ append(it.title + "\n") }, it.title, it.url)
                }
                descTextView.text = span
            }
        }

        // Clickable links that pass back URLs
        private inline fun SpannableStringBuilder.click(
            builderAction: SpannableStringBuilder.() -> Unit,
            title: String,
            url: String
        ) =
            inSpans(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        Log.d(CharacterListAdapter::class.simpleName, "Link $url was clicked ")
                        onUrlClicked.invoke(title, url)
                    }
                },
                builderAction = builderAction
            )
    }

    private class PeopleDiffCallback : DiffUtil.ItemCallback<Character>() {
        override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
            return oldItem == newItem
        }
    }
}