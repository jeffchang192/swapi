package io.jeffchang.swapi

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import io.jeffchang.characters.CharactersClient
import io.jeffchang.characters.GetCharactersCallback
import io.jeffchang.core.data.model.Character
import io.jeffchang.core.util.APIError
import io.jeffchang.swapi.databinding.ActivityCharacterBinding

private val TAG = CharacterActivity::class.simpleName

private const val LAST_SEARCH_QUERY = "LAST_SEARCH_QUERY"

private const val DEFAULT_QUERY = "Darth"


class CharacterActivity : AppCompatActivity() {

    private lateinit var adapter: CharacterListAdapter

    private lateinit var binding: ActivityCharacterBinding

    private val charactersClient: CharactersClient by lazy {
        CharactersClient()
    }

    private var isUIEnabled: Boolean = true
        set(value) {
            binding.apply {
                searchView.isEnabled = value
                recyclerView.visibility = if (value) View.VISIBLE else View.GONE
                progressBar.visibility = if (value) View.GONE else View.VISIBLE
            }
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCharacterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initPeopleList()
        initSearch(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, binding.searchView.query.trim().toString())
    }

    private fun searchPeople(term: String) {
        isUIEnabled = false
        charactersClient.getCharacters(term, object : GetCharactersCallback {

            override fun onComplete() {
                isUIEnabled = true
            }

            override fun onSuccess(people: List<Character>) {
                Log.d(TAG, "Received people list")
                adapter.submitList(people)
            }

            override fun onFailure(error: APIError) {
                Log.e(TAG, error.name)
                val text = when (error) {
                    APIError.DATA_MISSING -> {
                        R.string.no_people_found
                    }
                    APIError.NETWORK -> {
                        R.string.network_issue
                    }
                    APIError.UNKNOWN -> {
                        R.string.unexpected_issue
                    }
                    APIError.RATE_LIMIT_EXCEEDED -> {
                        R.string.limit_exceeded
                    }
                }
                Toast.makeText(
                    this@CharacterActivity,
                    text,
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
        })
    }

    private fun navigateToFilmFragment(title: String, url: String) {
        FilmCharacterActivity.startActivity(this, title, url)
    }

    private fun initSearch(savedInstanceState: Bundle?) {
        val search = binding.searchView
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrBlank()) {
                    return false
                }
                searchPeople(query)
                search.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // no-op
                return false
            }
        })

        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY)
        binding.searchView.setQuery(query ?: DEFAULT_QUERY, true)
    }

    private fun initPeopleList() {
        binding.apply {
            recyclerView.adapter = CharacterListAdapter(::navigateToFilmFragment).also {
                adapter = it
            }
            recyclerView.layoutManager = LinearLayoutManager(this@CharacterActivity)
        }
    }
}