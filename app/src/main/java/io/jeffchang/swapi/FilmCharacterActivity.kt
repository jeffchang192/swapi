package io.jeffchang.swapi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.jeffchang.core.util.APIError
import io.jeffchang.films.FilmClient
import io.jeffchang.films.GetCharactersFromFilmCallback
import io.jeffchang.swapi.databinding.ActivityFilmCharacterBinding


private const val ARG_FILM_URL = "ARG_FILM_URL"
private const val ARG_FILM_TITLE = "ARG_FILM_TITLE"

private val TAG = FilmCharacterActivity::class.simpleName

class FilmCharacterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFilmCharacterBinding

    private val url: String by lazy {
        intent.getStringExtra(ARG_FILM_URL)
    }

    private val filmTitle: String by lazy {
        intent.getStringExtra(ARG_FILM_TITLE)
    }

    private val charactersClient: FilmClient by lazy {
        FilmClient()
    }

    private var isUIEnabled: Boolean = true
        set(value) {
            binding.apply {
                recyclerView.visibility = if (value) View.VISIBLE else View.GONE
                progressBar.visibility = if (value) View.GONE else View.VISIBLE
            }
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFilmCharacterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = filmTitle

        getCharactersFromFilm()
    }

    private fun initListView(characters: List<String>) {
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1, characters
        )
        val listView = binding.recyclerView
        listView.adapter = adapter
    }

    private fun getCharactersFromFilm() {
        isUIEnabled = false
        binding.apply {
            charactersClient.getCharactersFromFilm(url, object : GetCharactersFromFilmCallback {
                override fun onSuccess(characters: List<String>) {
                    initListView(characters)
                }

                override fun onFailure(error: APIError) {
                    Log.e(TAG, error.name)
                    val text = when (error) {
                        APIError.DATA_MISSING -> {
                            R.string.no_people_found
                        }
                        APIError.NETWORK -> {
                            R.string.network_issue
                        }
                        APIError.UNKNOWN -> {
                            R.string.unexpected_issue
                        }
                        APIError.RATE_LIMIT_EXCEEDED -> {
                            R.string.limit_exceeded
                        }
                    }
                    Toast.makeText(
                        this@FilmCharacterActivity,
                        text,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onComplete() {
                    isUIEnabled = true
                }

            })
        }
    }

    companion object {
        fun startActivity(context: Context, filmTitle: String, url: String) {
            val intent = Intent(context, FilmCharacterActivity::class.java)
            intent.putExtra(ARG_FILM_URL, url)
            intent.putExtra(ARG_FILM_TITLE, filmTitle)
            context.startActivity(intent)
        }
    }
}