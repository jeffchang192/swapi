## Vungle Mobile Project

**Build Instructions**

* Clone Project and Use at the minimum Android Studio 4.0

- Build in Android Studio. Should require nothing more out of the box.

### Setbacks
* Testing the callbacks was difficult for me coming from mostly working with RxJava 2 and
Coroutines testing.


### Features
* Android library to retrieve list of characters from a search query.
* Android library to retrieve list of characters from a film location.
* Sample app for libraries.

### Module Graph
Core is used by all modules
```mermaid
graph TD;
                 Data-->Characters;
                 Data-->Films;
```

### Your Focus Areas
Software Architecture, Code Style, Readability, using standard Android development practices, Clean Code.

The endpoint for the characters return the links to the film url's location on the API. Can easily have been done with RxJava's zip operator. I learned how to use Java 8's Futures APIs to chain these calls together while maintaining code readability.

I designed the modules to have an API service that would resemble a micro-service architecture on a backend system.

* Unit Tests
* Modularization
* Tested with Rotation
* Writing minimal API surfaces and hiding implementation.
* Writing API surfaces that express nullability
* Error handling

### What's Next
* Mechanism for caching and reduce duplication of calls for the same resource.
